import '../node_modules/bulma/bulma.sass';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import firebase from 'firebase';

Vue.config.productionTip = false;

var config = {
  apiKey: "AIzaSyD9SlZ5V_4KxAjqM7ooBFMmuewdMRlRIdY",
  authDomain: "chat-app-a3e54.firebaseapp.com",
  databaseURL: "https://chat-app-a3e54.firebaseio.com",
  projectId: "chat-app-a3e54",
  storageBucket: "chat-app-a3e54.appspot.com",
  messagingSenderId: "133317940462"
};

firebase.initializeApp(config)

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
